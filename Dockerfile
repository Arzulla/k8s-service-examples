FROM alpine:3.11.2
RUN apk add --no-cache openjdk11
COPY build/libs/k8s-spring-1.0.407983c.jar /app/
WORKDIR /app/
ENTRYPOINT ["java"]
CMD ["-jar", "/app/k8s-spring-1.0.407983c.jar"]
