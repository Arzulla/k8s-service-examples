package com.jabiyev.k8sspring.controller;

import java.util.Objects;

public class HelloDto {
    private String message;

    public HelloDto() {
    }

    public HelloDto(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public HelloDto setMessage(String message) {
        this.message = message;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HelloDto helloDto = (HelloDto) o;
        return Objects.equals(message, helloDto.message);
    }

    @Override
    public int hashCode() {
        return Objects.hash(message);
    }

}
